package tasks.exceptions;

/**
 * Custom exception for null date objects
 */
public class NullDateException extends RuntimeException{

    public NullDateException(String message) {
        super(message);
    }
}
