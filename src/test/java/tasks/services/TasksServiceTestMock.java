package tasks.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import tasks.model.ArrayTaskList;
import tasks.model.Task;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

class TasksServiceTestMock {

    @Mock
    private ArrayTaskList tasks;

    @InjectMocks
    private TasksService service;

    final Date MAY11_2022_DATE = new GregorianCalendar(2022, Calendar.MAY, 11).getTime();
    final Date MAY12_2022_DATE = new GregorianCalendar(2022, Calendar.MAY, 12).getTime();

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testAddTask_NegativeInterval_ExceptionThrown() {
        assertThrows(IllegalArgumentException.class, () -> {
            Task t = new Task("task",MAY11_2022_DATE,MAY12_2022_DATE, -30000);
            Mockito.doThrow(IllegalArgumentException.class).when(tasks).add(t);
            service.addTask(t);
            Mockito.verify(tasks, times(1)).add(t);
        }, "Interval should be positive");
    }

    @Test
    void testAddTask_ValidTask_AddedSuccessfully() {
        Task t = new Task("task",MAY11_2022_DATE,MAY12_2022_DATE, 30000);
        Mockito.when(tasks.getAll()).thenReturn(Arrays.asList(t));
        Mockito.when(tasks.size()).thenReturn(1);
        Mockito.doNothing().when(tasks).add(t);
        service.addTask(t);

        Mockito.verify(tasks, times(1)).add(t);
        Mockito.verify(tasks, never()).getAll();

        assertEquals(1, tasks.size());
        assertEquals("task", tasks.getAll().get(0).getDescription());
        assertEquals(30000, tasks.getAll().get(0).getRepeatInterval());

        Mockito.verify(tasks, times(2)).getAll();
        Mockito.verify(tasks, times(1)).size();
    }




    @AfterEach
    void tearDown() {
    }


}