package tasks.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tasks.model.ArrayTaskList;
import tasks.model.Task;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class TasksServiceTest {
    private TasksService service;

    final Date MAY10_2022_DATE = new GregorianCalendar(2022, Calendar.MAY, 10).getTime();
    final Date MAY11_2022_DATE = new GregorianCalendar(2022, Calendar.MAY, 11).getTime();
    final Date MAY12_2022_DATE = new GregorianCalendar(2022, Calendar.MAY, 12).getTime();
    final Date MAY13_2022_DATE = new GregorianCalendar(2022, Calendar.MAY, 13).getTime();
    final Date MAY14_2022_DATE = new GregorianCalendar(2022, Calendar.MAY, 14).getTime();

    @BeforeEach
    void setUp() {
        service =  new TasksService(new ArrayTaskList());
        service.addTask(new Task("task1",MAY10_2022_DATE,MAY11_2022_DATE,10000));
        service.addTask(new Task("task2",MAY11_2022_DATE,MAY12_2022_DATE,10000));
        service.addTask(new Task("task3",MAY12_2022_DATE,MAY13_2022_DATE,10000));
        service.addTask(new Task("task4",MAY13_2022_DATE,MAY14_2022_DATE,10000));
    }

    @Test
    void testAddTask_NegativeInterval_ExceptionThrown() {
        assertThrows(IllegalArgumentException.class, () -> {
            service.addTask(new Task("task5",MAY11_2022_DATE,MAY12_2022_DATE, -30000));
        }, "Interval should be positive");
    }

    @Test
    void testFilterTasks() {
        Iterable<Task> filteredTasks = service.filterTasks(MAY11_2022_DATE,MAY12_2022_DATE);
        List<Task> taskList = new ArrayList<>();
        filteredTasks.forEach(taskList::add);
        assertEquals(2,taskList.size());
        assertEquals("task2",taskList.get(0).getDescription());
        assertEquals("task3",taskList.get(1).getDescription());
    }

    @AfterEach
    void tearDown() {
    }


}