package tasks.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tasks.exceptions.NullDateException;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TasksOperationsTest {

    TasksOperations tasksOperations;

    final Date MAY10_2022_DATE = new GregorianCalendar(2022, Calendar.MAY, 10).getTime();
    final Date MAY11_2022_DATE = new GregorianCalendar(2022, Calendar.MAY, 11).getTime();
    final Date MAY12_2022_DATE = new GregorianCalendar(2022, Calendar.MAY, 12).getTime();
    final Date MAY13_2022_DATE = new GregorianCalendar(2022, Calendar.MAY, 13).getTime();
    final Date MAY14_2022_DATE = new GregorianCalendar(2022, Calendar.MAY, 14).getTime();

    @BeforeEach
    void setUp() {
        final Task t = new Task("Test task", MAY12_2022_DATE);
        final ObservableList<Task> obsList = FXCollections.observableArrayList();
        obsList.add(t);

        tasksOperations = new TasksOperations(obsList);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testGetTasksInInterval_nullStart_nullEnd_throwsException() {
        assertThrows(NullDateException.class, () -> {
            tasksOperations.getTasksInInterval(null, null);
        });
    }

    @Test
    void testGetTasksInInterval_nullStart_throwsException() {
        assertThrows(NullDateException.class, () -> {
            tasksOperations.getTasksInInterval(null, MAY13_2022_DATE);
        });
    }

    @Test
    void testGetTasksInInterval_nullEnd_throwsException() {
        assertThrows(NullDateException.class, () -> {
            tasksOperations.getTasksInInterval(MAY10_2022_DATE, null);
        });
    }

    @Test
    void testGetTasksInInterval_allTasksBeforeStart_emptyListReturned() {
        assert (!tasksOperations.getTasksInInterval(MAY13_2022_DATE, MAY14_2022_DATE).iterator().hasNext());
    }

    @Test
    void testGetTasksInInterval_taskEqualsEndDate_returnsEndDateTask() {
        assert (tasksOperations.getTasksInInterval(MAY10_2022_DATE, MAY12_2022_DATE).iterator().next().getStartTime().equals(MAY12_2022_DATE));
    }

    @Test
    void testGetTasksInInterval_taskListSize2_returnsAllTasks() {
        tasksOperations = getTaskOperationsWithDate(2, MAY12_2022_DATE);

        final int[] taskReturnedNum = {0};
        tasksOperations.getTasksInInterval(MAY10_2022_DATE, MAY14_2022_DATE).iterator().forEachRemaining((i) -> {
            taskReturnedNum[0]++;
        });

        assertEquals(2, taskReturnedNum[0]);
    }


    @Test
    void testGetTasksInInterval_taskListSize10_returnsEmptyList() {
        tasksOperations = getTaskOperationsWithDate(10, MAY12_2022_DATE);

        final int[] taskReturnedNum = {0};
        tasksOperations.getTasksInInterval(MAY10_2022_DATE, MAY11_2022_DATE).iterator().forEachRemaining((i) -> {
            taskReturnedNum[0]++;
        });

        assertEquals(0, taskReturnedNum[0]);
    }

    @Test
    void testGetTasksInInterval_taskListEmpty_returnsEmptyList() {
        tasksOperations = getTaskOperationsWithDate(0, MAY12_2022_DATE);

        assert (!tasksOperations.getTasksInInterval(MAY10_2022_DATE, MAY11_2022_DATE).iterator().hasNext());
    }


    private TasksOperations getTaskOperationsWithDate(int taskNum, Date dateOfTasks) {
        final ObservableList<Task> obsList = FXCollections.observableArrayList();
        for (int i = 0; i < taskNum; ++i) {
            final Task t = new Task("Test task" + (i + 1), dateOfTasks);
            obsList.add(t);
        }

        return new TasksOperations(obsList);
    }
}