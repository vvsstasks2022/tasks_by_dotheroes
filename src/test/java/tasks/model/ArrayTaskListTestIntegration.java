package tasks.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import tasks.services.TasksService;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

class ArrayTaskListTestIntegration {

    @Mock
    private Task task;

    @InjectMocks
    private ArrayTaskList taskList;

    private TasksService service;

    private Date startDate;
    private Date endDate;

    private static final String VALID_DESCRIPTION = "Description";
    private static final String VALID_DESCRIPTION_LENGTH1 = "M";
    private static final String EMPTY_DESCRIPTION = "";
    private static final int VALID_INTERVAL = 0;
    private static final int NEGATIVE_INTERVAL = -1;
    private static final int MAX_INT = 2147483647;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        taskList = new ArrayTaskList();
        service = new TasksService(taskList);
        startDate = new Date(10000);
        endDate = new Date(20000);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testAddTask_ValidTask_AddedSuccessfully() {
        Mockito.when(task.getDescription()).thenReturn(VALID_DESCRIPTION);
        Mockito.when(task.getRepeatInterval()).thenReturn(VALID_INTERVAL);
        Mockito.when(task.getStartTime()).thenReturn(startDate);
        Mockito.when(task.getEndTime()).thenReturn(endDate);
        service.addTask(task);

        assertEquals(1, service.getObservableList().size());
        assertEquals(VALID_DESCRIPTION, service.getObservableList().get(0).getDescription());
        assertEquals(VALID_INTERVAL, service.getObservableList().get(0).getRepeatInterval());

        assertEquals(1, taskList.size());
        assertEquals(VALID_DESCRIPTION, taskList.getTask(0).getDescription());
        assertEquals(VALID_INTERVAL, taskList.getTask(0).getRepeatInterval());

        Mockito.verify(task, times(3)).getDescription();   //metoda este apelata si in taskList.add(task)
        Mockito.verify(task, times(2)).getRepeatInterval();
        Mockito.verify(task, never()).getStartTime();
        Mockito.verify(task, never()).getEndTime();
    }

    @Test
    void testAddTask_IntervalMaxValue_AddedSuccessfully() {
        Mockito.when(task.getDescription()).thenReturn(VALID_DESCRIPTION);
        Mockito.when(task.getRepeatInterval()).thenReturn(MAX_INT);
        Mockito.when(task.getStartTime()).thenReturn(startDate);
        Mockito.when(task.getEndTime()).thenReturn(endDate);
        taskList.add(task);

        assertEquals(1, service.getObservableList().size());
        assertEquals(MAX_INT, service.getObservableList().get(0).getRepeatInterval());

        assertEquals(1, taskList.size());
        assertEquals(MAX_INT, taskList.getTask(0).getRepeatInterval());

        Mockito.verify(task, times(1)).getDescription();   //metoda este apelata in taskList.add(task)
        Mockito.verify(task, times(2)).getRepeatInterval();
        Mockito.verify(task, never()).getStartTime();
        Mockito.verify(task, never()).getEndTime();
    }

}