package tasks.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ArrayTaskListTest {

    private ArrayTaskList taskList;
    private Date startDate1;
    private Date endDate1;

    private static final String VALID_DESCRIPTION = "Description";
    private static final String VALID_DESCRIPTION_LENGTH1 = "M";
    private static final String EMPTY_DESCRIPTION = "";
    private static final int VALID_INTERVAL = 0;
    private static final int NEGATIVE_INTERVAL = -1;
    private static final int MAX_INT = 2147483647;

    @BeforeEach
    void setUp() {
        taskList = new ArrayTaskList();
        startDate1 = new Date(10000);
        endDate1 = new Date(20000);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testAddTask_ValidTask_AddedSuccessfully() {
        Task t = new Task(VALID_DESCRIPTION, startDate1, endDate1, VALID_INTERVAL);
        taskList.add(t);

        assertEquals(1, taskList.size());
        assertEquals(VALID_DESCRIPTION, taskList.getTask(0).getDescription());
        assertEquals(VALID_INTERVAL, taskList.getTask(0).getRepeatInterval());
    }

    @Test
    void testAddTask_NegativeInterval_ExceptionThrown() {
        assertThrows(IllegalArgumentException.class, () -> {
            taskList.add(new Task(VALID_DESCRIPTION, startDate1, endDate1, NEGATIVE_INTERVAL));
        }, "Interval should be positive");
    }

    @Test
    void testAddTask_EmptyDescription_ExceptionThrown() {
        assertThrows(IllegalArgumentException.class, () -> {
            taskList.add(new Task(EMPTY_DESCRIPTION, startDate1, endDate1, VALID_INTERVAL));
        }, "Description can not be empty");
    }

    @Test
    void testAddTask_DescriptionLength1_AddedSuccessfully() {
        Task t = new Task(VALID_DESCRIPTION_LENGTH1, startDate1, endDate1, VALID_INTERVAL);

        taskList.add(t);

        assertEquals(VALID_DESCRIPTION_LENGTH1, taskList.getTask(0).getDescription());
    }

    @Test
    void testAddTask_IntervalMaxValue_AddedSuccessfully() {
        taskList.add(new Task(VALID_DESCRIPTION, startDate1, endDate1, MAX_INT));

        assertEquals(MAX_INT, taskList.getTask(0).getRepeatInterval());
    }
}