package tasks.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tasks.services.TasksService;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class TaskTestIntegration {

    private TasksService service;
    private ArrayTaskList taskList;
    private Date startDate;
    private Date endDate;

    private static final String VALID_DESCRIPTION = "Description";
    private static final String VALID_DESCRIPTION_LENGTH1 = "M";
    private static final String EMPTY_DESCRIPTION = "";
    private static final int VALID_INTERVAL = 0;
    private static final int NEGATIVE_INTERVAL = -1;
    private static final int MAX_INT = 2147483647;

    @BeforeEach
    void setUp() {
        taskList = new ArrayTaskList();
        service = new TasksService(taskList);
        startDate = new Date(10000);
        endDate = new Date(20000);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testAddTask_ValidTask_AddedSuccessfully() {
        Task t = new Task(VALID_DESCRIPTION, startDate, endDate, VALID_INTERVAL);
        service.addTask(t);

        assertEquals(1, service.getObservableList().size());
        assertEquals(VALID_DESCRIPTION, service.getObservableList().get(0).getDescription());
        assertEquals(VALID_INTERVAL, service.getObservableList().get(0).getRepeatInterval());

        assertEquals(1, taskList.size());
        assertEquals(VALID_DESCRIPTION, taskList.getTask(0).getDescription());
        assertEquals(VALID_INTERVAL, taskList.getTask(0).getRepeatInterval());
    }

    @Test
    void testAddTask_NegativeInterval_ExceptionThrown() {
        assertThrows(IllegalArgumentException.class, () -> {
            service.addTask(new Task(VALID_DESCRIPTION, startDate, endDate, NEGATIVE_INTERVAL));
        }, "Interval should be positive");

        assertEquals(0,taskList.getAll().size());
    }

}